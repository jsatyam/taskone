//
//  ViewController.swift
//  TaskOne
//
//  Created by Jyotiraditya Satyam on 22/01/22.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - IBOutlet properties
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gridCollectionView: UICollectionView!
    
    // MARK: - Instance properties
    private var timer: Timer?
    private var showIndex = -1
    private var numberOfItem = 0
    private var selectedGrid: [Int] = []
    private var isShowSelectionIndex = false
    
    // MARK: - View controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - IBAction methods
    @IBAction func submitButtonAction(_ sender: UIButton) {
        guard let text = inputTextField.text, let number = Double(text) else { return }
        let root = sqrt(number)
        let isInteger = floor(root) == root
        if isInteger {
            numberOfItem = Int(number)
            gridCollectionView.reloadData()
            startRandomSeconds()
        } else {
            numberOfItem = 0
            gridCollectionView.reloadData()
        }
    }
    
    // MARK: - Private helper methods
    private func startRandomSeconds() {
        timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { [weak self] timer in
            guard let self = self else { return }
            let index = Int(arc4random_uniform(UInt32(self.numberOfItem)))
            let isSelected = self.selectedGrid.firstIndex(of: index) != nil
            if !isSelected, !self.isShowSelectionIndex {
                self.showIndex = index
                self.isShowSelectionIndex = true
                self.gridCollectionView.reloadData()
            }
        }
    }
    
    private func showAlert(title: String = "Alert", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.numberOfItem = 0
            self.gridCollectionView.reloadData()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - UITextField delegate
extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Check for invalid input characters
        if !CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) {
            return false
        }
        
        return true
    }
}

// MARK: - UICollection View dataSource
extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        numberOfItem
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "grid", for: indexPath)
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.gray.cgColor
        let isSelected = selectedGrid.firstIndex(of: indexPath.row) != nil
        let bgColor = isSelected ? UIColor.red : UIColor.white
        cell.backgroundColor = bgColor
        if showIndex == indexPath.row {
            cell.backgroundColor = .green
        }
        return cell
    }
}

// MARK: - UICollection View delegate
extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if showIndex == indexPath.row, isShowSelectionIndex {
            showIndex = -1
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = .red
            isShowSelectionIndex = false
            selectedGrid.append(indexPath.row)
            gridCollectionView.reloadData()
        }
        
        if self.numberOfItem == self.selectedGrid.count {
            timer?.invalidate()
            showAlert(message: "Wow!, You did it!")
        }
    }
}
